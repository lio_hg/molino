<?php

function decodificadorMorse($textoMorse)
{
    $abecedarioMorse = [
        'a' => '.-',
        'b' => '-...',
        'c' => '-.-.',
        'd' => '-..',
        'e' => '.',
        'f' => '..-.',
        'g' => '--.',
        'h' => '....',
        'i' => '..',
        'j' => '.---',
        'k' => '-.-',
        'l' => '.-..',
        'm' => '--',
        'n' => '-.',
        'ñ' => '--.--',
        'o' => '---',
        'p' => '.--.',
        'q' => '--.-',
        'r' => '.-.',
        's' => '...',
        't' => '-',
        'u' => '..-',
        'v' => '...-',
        'w' => '.--',
        'x' => '-..-',
        'y' => '-.--',
        'z' => '--..',
        '0' => '-----',
        '1' => '.----',
        '2' => '..---',
        '3' => '...--',
        '4' => '....-',
        '5' => '.....',
        '6' => '-....',
        '7' => '--...',
        '8' => '---..',
        '9' => '----.',
        '.' => '.-.-.-',
        ',' => '--..--',
        ':' => '---...',
        "'" => '.----.',
        '-' => '-....-',
        '/' => '-..-.',
        '(' => '-.--.',
        ')' => '-.--.-',
        '"' => '.-..-.',
        '=' => '-...-'
    ];

    $palabrasMorse = explode('  ', $textoMorse);
    $palabrasNormales = [];
    foreach ($palabrasMorse as $palabraMorse) {
        $caracteresMorse = explode(' ', $palabraMorse);

        $palabraNormal = '';
        foreach ($caracteresMorse as $caracterMorse) {
            $caracterNormal = array_search(trim($caracterMorse), $abecedarioMorse);
            if ($caracterNormal === false) {
                continue;
            }
            $palabraNormal .= $caracterNormal;
        }

        $palabrasNormales[] = $palabraNormal;
    }

    return implode(' ', $palabrasNormales);
}

$input = '';
while ($f = fgets(STDIN)) {
    $input .= $f;
}

$lineas = explode(PHP_EOL, $input);


$output = '';
foreach ($lineas as $i => $linea) {
    if (!empty($linea)) {
        $output .= decodificadorMorse($linea);
    }

    if ($i < count($lineas) - 1) {
        $output .= PHP_EOL;
    }
}

echo $output;



