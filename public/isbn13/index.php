<?php

function isbn13Valido($isbn)
{
    if (strpos(trim($isbn), '-') !== false && strpos(trim($isbn), ' ') !== false) {
        return 'INCORRECTO';
    }

    if (strpos(trim($isbn), '--') !== false || strpos(trim($isbn), '  ') !== false) {
        return 'INCORRECTO';
    }

    $posicionesDeSeparadores = [3, 6, 10, 15];
    if (strpos(trim($isbn), '-') !== false || strpos(trim($isbn), ' ') !== false) {
        foreach ($posicionesDeSeparadores as $pos){
            if($isbn[$pos] !== '-' && $isbn[$pos] !== ' '){
                return 'INCORRECTO';
            }
        }
    }

    $isbnLimpio = str_replace(' ', '', $isbn);
    $isbnLimpio = str_replace('-', '', $isbnLimpio);
    $isbnLimpio = trim($isbnLimpio);

    if (strlen($isbnLimpio) != 13) {
        return 'INCORRECTO';
    }

    $isbnDocePrimerosDigitos = str_split(substr($isbnLimpio, 0, 12));
    $isbnUltimoDigito = substr($isbnLimpio, 12, 1);

    $isbnSuma = 0;
    foreach ($isbnDocePrimerosDigitos as $i => $digito) {
        if ($i % 2 == 0) {
            $multiplicador = 1;
        } else {
            $multiplicador = 3;
        }

        $isbnSuma += $digito * $multiplicador;
    }

    $r = $isbnSuma % 10;
    if ($r == 0) {
        $dc = 0;
    } else {
        $dc = 10 - $r;
    }
    return $dc == $isbnUltimoDigito ? 'CORRECTO' : 'INCORRECTO';
}

$input = '';
while ($f = fgets(STDIN)) {
    $input .= $f;
}

$lineas = explode(PHP_EOL, $input);
$n = array_shift($lineas);

if (($n < 0 || $n > 10)) {
    echo 'INCORRECTO';
    die();
}

$output = '';
foreach ($lineas as $i => $linea) {
    if (!empty($linea)) {
        $output .= isbn13Valido($linea);
    }

    if ($i < count($lineas) - 1) {
        $output .= PHP_EOL;
    }
}

echo $output;



