<?php

function numeroTarjetaValido($numeroTarjeta)
{
    $ch = curl_init('http://www.genware.es/xValTarjetaCredito.php');
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, 'tarjeta=' . $numeroTarjeta);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Referer: http://www.genware.es/index.php?ver=tarjetascredito',
    ));
    $respuesta = curl_exec($ch);
    curl_close($ch);

    $resultadoRespuesta = $respuesta[0];

    return $resultadoRespuesta == '1' ? 'VALIDO' : 'NO valido';
}

$input = '';
while ($f = fgets(STDIN)) {
    $input .= $f;
}

$lineas = explode(PHP_EOL, $input);

$output = '';
foreach ($lineas as $i => $linea) {
    if (!empty($linea)) {
        $output .= numeroTarjetaValido($linea);
    }

    if ($i < count($lineas) - 1) {
        $output .= PHP_EOL;
    }
}

echo $output;